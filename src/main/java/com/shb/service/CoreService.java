package com.shb.service;

import com.shb.model.Sgat9460K001;
import com.shb.model.Sgat9461K001;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CoreService {

  private static final Logger logger = LoggerFactory.getLogger(CoreService.class);

  @Autowired
  ProducerTemplate producer;

  @SuppressWarnings("unused")
  public Sgat9461K001 process(Sgat9460K001 sgat9460K001) throws Exception {

    Sgat9461K001 sgat9461K001 =
        producer.requestBody("direct:coreSimulator", sgat9460K001, Sgat9461K001.class);

    return sgat9461K001;
  }
}
