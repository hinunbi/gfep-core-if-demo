package com.shb.util;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.freemarker.FreemarkerConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class XmlLoader {

  @Autowired
  ProducerTemplate producer;

  <T> T load(String template, Class<T> type) throws Exception {

    T xml = producer.requestBodyAndHeader(
        "direct:loadXml",
        "",
        FreemarkerConstants.FREEMARKER_RESOURCE_URI,
        template,
        type);

    return xml;
  }
}
