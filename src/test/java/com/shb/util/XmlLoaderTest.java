package com.shb.util;

import com.shb.Application;
import com.shb.model.Sgat9460K001;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = Application.class)
public class XmlLoaderTest {

  @Autowired
  XmlLoader xmlLoader;

  @Test
  public void load() throws Exception {

    Sgat9460K001 result = xmlLoader.load(
        "template/SGAT9460K001.xml",
        Sgat9460K001.class);

    Assert.isInstanceOf(Sgat9460K001.class, result);

  }
}