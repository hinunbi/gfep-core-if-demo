package com.shb.type;


import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@RunWith(CamelSpringRunner.class)
@ContextConfiguration(locations =
    {"file:src/test/java/com/shb/type/GroovyTest.xml"})
public class GroovyTest {

  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Autowired
  CamelContext camelContext;

  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Autowired
  ProducerTemplate producer;


  @Test
  public void groovy() {
    producer.requestBody("direct:groovy", "<in-xml>M</in-xml>");
  }
}
